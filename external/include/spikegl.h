// Copyright (c)  Vidrio Technologies, All Rights Reserved
// Author: Nathan Clack <nathan@vidriotech.com>

#ifndef H_VIDRIO_IO_SPIKEGL_V0
#define H_VIDRIO_IO_SPIKEGL_V0

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

#define spiegel_export __declspec(dllexport)

#include <stdint.h>

struct spikegl_dims { 
    int64_t channels,time;
};

struct spikegl_roi { 
    struct spikegl_dims offset,
                        shape;
};

struct spikegl_future { uint8_t private[128]; };

struct spikegl_reader { void* self; };

struct spikegl_logger {
    void (*logger)(const struct spikegl_logger *self,int is_error,const char *file,int line,const char* function,const char *fmt,...);
};

struct spikegl_reader_ctx {
    struct spikegl_logger logger;
    void *workspace; 
    size_t bytesof_workspace;
};

/** The default context will allocate the workspace and write any log messages
 *  to a buffer pointed to by `log`.  If no messages have been written yet,
 *  `log` will be NULL.
 * 
 *  All allocations are performed with malloc.
 * 
 *  The lifetime of the context needs to exceed that of the reader.
 */

struct spikegl_reader_default_ctx {
    union {
        struct spikegl_reader_ctx ctx;
        uint8_t private[64];        
    };
};

enum spikegl_status {
    spikegl_status_ok=0,
    spikegl_status_err
};

typedef enum spikegl_status spikegl_status_t;

spiegel_export
const char*           spikegl_reader_api_version();

spiegel_export
struct spikegl_reader_ctx* spikegl_default_context_create (size_t bytesof_workspace);

spiegel_export
void                  spikegl_default_context_destroy(struct spikegl_reader_ctx* ctx);

spiegel_export
const char*           spikegl_default_context_log    (struct spikegl_reader_ctx* ctx,
                                                      size_t *nbytes);

spiegel_export
spikegl_status_t      spikegl_reader_create    (struct spikegl_reader *r,
                                                const struct spikegl_reader_ctx *ctx);

spiegel_export
void                  spikegl_reader_destroy   (struct spikegl_reader *r);

spiegel_export
spikegl_status_t      spikegl_reader_open      (struct spikegl_reader *r,
                                                const char *path, const char* stream);

spiegel_export
spikegl_status_t      spikegl_reader_shape     (const struct spikegl_reader *r,
                                                struct spikegl_roi *bounds);

spiegel_export
size_t                spikegl_reader_nbytes    (const struct spikegl_roi *roi);

spiegel_export
struct spikegl_dims   spikegl_reader_strides   (const struct spikegl_roi *roi);

spiegel_export
spikegl_status_t      spikegl_reader_read      (struct spikegl_reader *r,
                                                const struct spikegl_roi *roi,
                                                uint16_t *buf,
                                                size_t bytesof_buf);

spiegel_export
spikegl_status_t      spikegl_reader_read_async(struct spikegl_future *future,
                                                struct spikegl_reader *r,
                                                const struct spikegl_roi *roi,
                                                uint16_t *buf,
                                                size_t bytesof_buf);

spiegel_export
spikegl_status_t      spikegl_await            (struct spikegl_future *f,
                                                struct spikegl_roi *roi,
                                                void** buf,
                                                size_t* nbytes);

spiegel_export
unsigned              spikegl_reader_ntrials   (struct spikegl_reader *r);

spiegel_export
spikegl_status_t      spikegl_reader_get_roi   (struct spikegl_reader *r,
                                                struct spikegl_roi *roi,
                                                unsigned itrial);

#undef spiegel_export

#ifdef __cplusplus
} // end extern "C"
#endif //__cplusplus

#endif // H_VIDRIO_IO_SPIKEGL_V0