import sys
import spiegel.io.spikegl as spikegl
import os
import pytest

from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv(raise_error_if_not_found=True))

def test_api_version():
    assert type(spikegl.api_version())==str

def test_open_none():
    with pytest.raises(TypeError):
        spikegl.open(None)
        
def test_open_non_existent():
    with pytest.raises(IOError):
        # open is async, so it doesn't know the path doesn't exist.
        # need to do a query, to force the check.
        spikegl.open("does-not-exist").bounds()

def test_open():
    # open is async, so it doesn't know the whether the path exists.
    with spikegl.open(os.getenv("SPIEGEL_IO_TEST_DATA")) as reader:
        assert reader

def test_shape():
    with spikegl.open(os.getenv("SPIEGEL_IO_TEST_DATA")) as reader:
        (samp0,chan0),(nsamps,nchan)=reader.bounds()
        assert nchan==256
        assert nsamps==153889831
        assert samp0==43289162

def test_shape_fails_for_closed_reader():
    r=spikegl.open(os.getenv("SPIEGEL_IO_TEST_DATA"))
    r.close()
    with pytest.raises(RuntimeError):
        r.bounds()

def test_read():
    from spiegel.io.spikegl import Roi,Dimensions
    roi=Roi(Dimensions(64,44556646+10000),Dimensions(64,1000000))
    with spikegl.open(os.getenv("SPIEGEL_IO_TEST_DATA")) as reader:
        a=reader.read(roi)
        assert a.min()!=0
        assert a.max()!=0


