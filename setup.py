__author__    = "Nathan Clack <nathan@vidriotech.com>"
__copyright__ = "Copyright (C) 2018 Vidrio Technologies"
__license__   = """\
This work is licensed under a
Creative Commons Attribution-NoDerivatives 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nd/4.0/>.
"""

from setuptools import setup,find_packages
import os
import sys

from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to pytest")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = ''

    def run_tests(self):
        import shlex        
        import pytest #import here, because outside the eggs aren't loaded
        errno = pytest.main(shlex.split(self.pytest_args))
        sys.exit(errno)

def git_description():
    """\
    Requires git to be on the path.
    Requires the repository to have a tag.
    """
    import subprocess    
    def hash():
        res=subprocess.run("git describe --always".split(),stdout=subprocess.PIPE)
        if res.returncode==0:
            return res.stdout.decode("utf-8").strip()
        else:
            return "unknown"
    def tag():
        res=subprocess.run("git describe --tags --abbrev=0".split(),stdout=subprocess.PIPE)
        if res.returncode==0:
            return res.stdout.decode("utf-8").strip().strip('v')
        else:
            return ""
    return "+".join(filter(lambda s: len(s)>0,[tag(),hash()]))



setup(
    name='spiegel-io-spikegl',
    url="https://gitlab.com/vidriotech/spiegel/io_spikegl_python3", 
    author="Nathan Clack",
    author_email="nathan@vidriotech.com",
    version=git_description(),
    packages=find_packages('src',exclude=['contrib', 'docs', 'tests']),
    license='CC BY-NC-ND 4.0',
    long_description=open('README.rst',encoding='utf-8').read(),    
    package_dir={'':'src'},
    package_data={
        'spiegel':[os.path.join(os.path.dirname(os.path.abspath(__file__)),'external','lib','io_spikegl.dll')]
        },
    install_requires=['numpy'],
    tests_require=['pytest'],    
    cmdclass = {'test': PyTest},
    project_urls={
        # 'Documentation': '',
        #'Funding': '',
        #'Say Thanks!': 'http://saythanks.io/to/example',
        'Source': 'https://gitlab.com/vidriotech/spiegel/io_spikegl_python3',
        'Tracker': 'https://gitlab.com/vidriotech/spiegel/io_spikegl_python3/issues',
    },
)