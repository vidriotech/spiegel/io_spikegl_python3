__author__    = "Nathan Clack <nathan@vidriotech.com>"
__copyright__ = "Copyright (C) 2018 Vidrio Technologies"
__license__   = """\
This work is licensed under a
Creative Commons Attribution-NoDerivatives 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nd/4.0/>.
"""

__doc__="""\
What
"""

from . import spikegl