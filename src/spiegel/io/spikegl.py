__author__    = "Nathan Clack <nathan@vidriotech.com>"
__copyright__ = "Copyright (C) 2018 Vidrio Technologies"
__license__   = """\
This work is licensed under a
Creative Commons Attribution-NoDerivatives 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nd/4.0/>.
"""

from pathlib import Path 
from ctypes import ( CDLL
                   , Structure
                   , c_char_p
                   , c_void_p
                   , c_long
                   , c_ulong
                   , c_size_t
                   , c_int64
                   , c_uint8
                   , c_int16
                   , Structure
                   , POINTER
                   , byref
                   )
from numpy import ( dtype
                  , empty
                  , int16
                  , ctypeslib
                  , ndarray
                  )

def _doctest_setup():
    # FIXME: I think there's a way to setup the doctest env via pytest
    #        so we don't have to do this here...it doesn't really belong here
    try:
        # The path to test data used in the doctests below can be stored in 
        # an environment variable or provided via a .env file.
        # The doctests then use the module-level global.        
        from dotenv import load_dotenv, find_dotenv
        import os
        load_dotenv(find_dotenv(raise_error_if_not_found=True))        
        globals()["SPIEGEL_IO_TEST_DATA"]=os.getenv("SPIEGEL_IO_TEST_DATA")
    except:
        pass
_doctest_setup()

#
# API DATATYPES (private)
#

class ReaderHandle(Structure):
    _fields_=[("private",c_void_p)]

class ReaderContext(Structure):
    _fields_=[("logger",c_void_p),
              ("workspace",c_void_p),
              ("bytesof_workspace",c_size_t)
              ]
    @staticmethod
    def default():
        '''Returns a default representation'''
        return ReaderContext(None,None,0)

    def log(self):
        '''Returns the message log used by the underlying c api to collect
        debug and error messages.
        
        :Example:

        >>> ReaderContext.default().log()
        ''
        
        '''
        n=c_size_t(0) # unused, could be used to double-check the buffer size
        b=_api.log(byref(self),byref(n))
        if b is None: return ''
        return b.decode('utf-8')

class Dimensions(Structure):
    _fields_=[("channels",c_int64),
              ("time",c_int64)
              ]
    @staticmethod
    def default():
        '''Returns a default representation'''
        return Dimensions(0,0)

    def __iter__(self):
        # for numpy compatibility, the fast axis goes last        
        yield self.time
        yield self.channels

    def to_roi(self):
        '''Converts the shape into an Roi.'''
        r=Roi.default()
        r.shape=self
        return r

class Roi(Structure):
    _fields_=[("offset",Dimensions),
              ("shape",Dimensions)
              ]

    @staticmethod
    def default():
        '''Returns a default representation'''
        return Roi(Dimensions.default(),Dimensions.default())

    def maybe_alloc(self,a):
        '''If a is an ndarray object that already meets requirements for this 
        Roi, then nothing happens and it is returned.

        Otherwise, a new array is allocated for the Roi and returned.
        '''
        if not (type(a)==ndarray 
            and a.dtype==int16 
            and a.shape==tuple(self.shape)):
            # Will attempt to reuse an array passed in a 'a', otherwise allocate:
            a=empty(tuple(self.shape),int16,'C') 
        return a

    def __iter__(self):
        yield self.offset
        yield self.shape

    def nbytes(self):
        '''Returns the number of bytes required to store data from the region
        covered by the Roi.

        :Example:

        >>> r=Roi(Dimensions(64,0),Dimensions(64,1000))
        >>> r.nbytes()
        128000
        '''
        return _api.nbytes(byref(self))

    def strides(self):
        '''Tuple of elements to step in each dimension when traversing an 
        array holding data for this Roi.

        For an array with data loaded for this roi, the element-wise offset of
        the element at `(sample,channel)` is:

            offset=sum([sample,channel]*roi.strides())

        :Example:

        >>> r=Roi(Dimensions(64,0),Dimensions(64,1000))
        >>> r.strides()
        (64, 1)
        '''
        st=_api.strides(byref(self))
        return st.time, st.channels
    
    def __getitem__(self,slcs):
        ''' Select (time,channel) range with a slice.
        
        Ignores the slice step.
        Returns a copy.  Does not modify self.

        :param slice-like slc: Specifies the channel range.  Must have `start` and `stop fields.
        :return: a new roi with the selected range.

        >>> from itertools import chain
        >>> r=Roi(Dimensions(0,0),Dimensions(256,1000))
        >>> tuple(chain.from_iterable(r[:,64:128]))
        (0, 64, 1000, 64)

        >>> tuple(chain.from_iterable(r[20:50]))
        (20, 0, 30, 256)
        '''

        t=slice(None,None,None) # this means "select all"
        c=slice(None,None,None)
        (t0,c0),(nt,nc)=self
        t1,c1=t0+nt,c0+nc
        if type(slcs)==slice: # the len==1 case
            t=slcs        
        elif len(slcs)==2:
            t,c=slcs
        else:
            raise IndexError(f'Only 1 or 2 indices supported (got {len(slcs)}).')
        t0=t.start if t.start else t0
        c0=c.start if c.start else c0
        nt=t.stop-t0 if t.stop else t1-t0
        nc=c.stop-c0 if c.stop else c1-c0
        return Roi(Dimensions(c0,t0),Dimensions(nc,nt))

class Future(Structure):
    _fields_=[("private",c_uint8*128)]

    def __init__(self,a):
        self.__array=a

    def _ecode(self,sts):
        if sts==spikegl_status_ok:
            return
        raise IOError("Read request failed")

    def wait(self):
        '''Blocks until the future is ready'''
        if self.__array is None:
            RuntimeError("Future can not be waited on multiple times.")
        ptr=c_void_p(0)
        nbytes=c_size_t(0)
        roi=Roi.default()
        self._ecode(_api.wait(byref(self),byref(roi),byref(ptr),byref(nbytes)))
        a=self.__array
        self.__array=None
        return a,roi

(spikegl_status_ok,spikegl_status_err)=(0,1)

#
# C API DECLARATIONS (private)
# Populate prototypes on module load
#

class API:
    def __init__(self):
        L=self.load()     
        self._api_lib=L

        self.version=L.spikegl_reader_api_version
        self.version.restype=c_char_p
        self.version.argtypes=None

        self.create_context=L.spikegl_default_context_create
        self.create_context.restype=POINTER(ReaderContext)
        self.create_context.argtypes=[c_size_t]

        self.destroy_context=L.spikegl_default_context_destroy
        self.destroy_context.restype=None
        self.destroy_context.argtypes=[POINTER(ReaderContext)]

        self.log=L.spikegl_default_context_log
        self.log.restype=c_char_p
        self.log.argtypes=[POINTER(ReaderContext),POINTER(c_size_t)]

        self.create=L.spikegl_reader_create
        self.create.restype=c_long
        self.create.argtypes=[POINTER(ReaderHandle),POINTER(ReaderContext)] # reader, context

        self.destroy=L.spikegl_reader_destroy
        self.destroy.restype=None
        self.destroy.argtypes=[POINTER(ReaderHandle)] # reader

        self.open =L.spikegl_reader_open
        self.open.restype=c_long
        self.open.argtypes=[POINTER(ReaderHandle),c_char_p,c_char_p] # reader, path

        self.shape=L.spikegl_reader_shape
        self.shape.restype=c_long
        self.shape.argtypes=[POINTER(ReaderHandle),POINTER(Roi)] # reader, shape(out)

        self.nbytes=L.spikegl_reader_nbytes
        self.nbytes.restype=c_size_t
        self.nbytes.argtypes=[POINTER(Roi)]

        self.strides=L.spikegl_reader_strides
        self.strides.restype=Dimensions
        self.strides.argtypes=[POINTER(Roi)]

        self.read=L.spikegl_reader_read
        self.read.restype=c_long
        self.read.argtypes=[POINTER(ReaderHandle),POINTER(Roi),POINTER(c_int16),c_size_t] # reader, roi, buf, bytesof_buf

        self.read_async=L.spikegl_reader_read_async
        self.read_async.restype=c_long
        self.read_async.argtypes=[POINTER(Future),POINTER(ReaderHandle),POINTER(Roi),POINTER(c_int16),c_size_t] # future, reader, roi, buf, bytesof_buf

        self.wait=L.spikegl_await
        self.wait.restype=c_long
        self.wait.argtypes=[POINTER(Future),POINTER(Roi),POINTER(c_void_p),POINTER(c_size_t)] # future, (out) roi, (out) buf, (out) nbytes

        self.ntrials=L.spikegl_reader_ntrials
        self.ntrials.restype=c_ulong # returns the trial count...
        self.ntrials.argtypes=[POINTER(ReaderHandle)] # reader

        self.roi=L.spikegl_reader_get_roi
        self.roi.restype=c_long
        self.roi.argtypes=[POINTER(ReaderHandle),POINTER(Roi),c_ulong] # reader, roi (out), trial_id

    @staticmethod
    def load():
        from os.path import (abspath,join,dirname)
        import platform
        name="io_spikegl.dll"
        if platform.system()!='Windows':
            name="io_spikegl.so"
        loadpath=join(dirname(abspath(__file__)),'..','..','..','external','lib',name)
        api=CDLL(loadpath)
        return api

def api_version():
    '''Returns a string stating the version of the c library that
    this python code is using.

    :Example:

    >>> import spiegel
    >>> print(spiegel.io.spikegl.api_version()) #doctest: +ELLIPSIS
    v...
    '''
    return _api.version().decode('utf-8')

class Reader:
    def __init__(self,path,stream=None):
        self._is_open=False    
        self.__path=path;    
        self.__ctx=_api.create_context(1<<15) # a pointer to a context
        self.__reader=ReaderHandle(None)      # not a pointer                
        self.open(path,stream)

    def __del__(self):
        ''' Closes the open file context releasing any bound resources. '''
        self.close()
        _api.destroy_context(self.__ctx)

    def __enter__(self):
        '''Support python's with syntax'''
        return self

    def __exit__(self,type,value,traceback):
        '''Support python's with syntax'''
        self.close()
        return False # re-raises the exception
    
    def _messages(self):
        return self.__ctx.contents.log()

    def _ecode(self,sts):
        if sts==spikegl_status_ok:
            return
        msg=self.__ctx.contents.log()
        # if msg:
        #     print(msg,flush=True)
        # else:
        #     print("MSG LOG: Empty",flush=True)
        raise IOError(self.__path)

    def open(self,path,stream=None):
        ''' Opens a SpikeGLX data set for reading.

        This open call is asynchronous.  It will not report an error if the 
        path doesn't exist.  Instead you'll see an exception later when a 
        query is performed.

        Called by the constructor.  Normally you don't need to use this
        function.  It's possible, however to reuse an instance of this class:

        :Example:

        >>> import os
        >>> reader=Reader(SPIEGEL_IO_TEST_DATA)
        >>> reader.close()
        >>> reader.open(SPIEGEL_IO_TEST_DATA)
        >>> print([tuple(e) for e in reader.bounds()])
        [(43289162, 0), (153889831, 256)]
        >>> reader.close()

        However, there's probably no real reason to use this pattern over just
        constructing new readers.
        '''
        path=Path(path)
        self.close() # just in case
        _api.create(byref(self.__reader),self.__ctx)
        if not stream:
            stream="nidq"
        self._ecode(_api.open(byref(self.__reader),bytes(path),stream.encode('utf-8')))
        self._is_open=True

    def close(self):
        ''' Closes the open Reader context releasing any bound resources. '''
        if self._is_open:
            _api.destroy(byref(self.__reader))
        self._is_open=False

    def _require_open(self):
        if not self._is_open:
            raise RuntimeError("Reader is not open")

    def bounds(self):
        '''Returns the smallest bounding box covering the full data set

        :Example:

        >>> with open(SPIEGEL_IO_TEST_DATA) as reader:
        ...     assert reader.bounds().shape.channels==256

        The returned Roi object is iterable:

        >>> from itertools import chain
        >>> tuple(chain.from_iterable(open(SPIEGEL_IO_TEST_DATA).bounds()))
        (43289162, 0, 153889831, 256)

        '''
        self._require_open()
        roi=Roi.default()
        self._ecode(_api.shape(byref(self.__reader),byref(roi)))
        return roi

    def read(self,roi,a=None):
        '''Read data for the `roi` into a numpy array.        

        :param roi: A `spiegel.io.spikegl.Roi` describing the region to read.
        :param   a: If None, a new numpy array will be constructed to hold the 
                    data.  Alternatively, a caller can supply an array.  If it
                    is the right size, that array will be used.

        :Example:

        >>> roi=Roi(Dimensions(64,0),Dimensions(64,1000))
        >>> with open(SPIEGEL_IO_TEST_DATA) as reader:
        ...     a=reader.read(roi)
        '''
        a=roi.maybe_alloc(a)  
              
        self._ecode(_api.read(
            byref(self.__reader),
            byref(roi),
            a.ctypes.data_as(POINTER(c_int16)),
            a.nbytes))
        return a

    def read_async(self,roi,a=None):
        '''Asynchronously read data for the `roi` into a numpy array.  

        Returns a Future that can be used to get the data later once it is 
        available.  The future can only be used one time.

        :param roi: A `spiegel.io.spikegl.Roi` describing the region to read.
        :param   a: If None, a new numpy array will be constructed to hold the 
                    data.  Alternatively, a caller can supply an array.  If it
                    is the right size, that array will be used.

        :Example:

        >>> roi=Roi(Dimensions(64,0),Dimensions(64,1000))
        >>> reader=open(SPIEGEL_IO_TEST_DATA)
        >>> f=reader.read_async(roi)
        >>> reader.close()
        >>> a,roi=f.wait()
        '''
        a=roi.maybe_alloc(a)
        future=Future(a)
        self._ecode(_api.read_async(
            byref(future),
            byref(self.__reader),
            byref(roi),
            a.ctypes.data_as(POINTER(c_int16)),
            a.nbytes))
        return future


    def trial_count(self):      
        '''
        Returns the number of trials recorded for the data set.

        :Example:

        >>> open(SPIEGEL_IO_TEST_DATA).trial_count()
        540
        '''
        self._require_open()
        return _api.ntrials(byref(self.__reader))

    def roi(self,trial_id=None):
        '''
        Returns the Roi for a specified trial

        :Example:

        >>> from itertools import chain
        >>> tuple(chain.from_iterable(open(SPIEGEL_IO_TEST_DATA).roi(8)))
        (45481064, 0, 197501, 256)
        '''
        self._require_open()
        if trial_id is None:
            return self.bounds()
        roi=Roi.default()
        self._ecode(_api.roi(byref(self.__reader),byref(roi),trial_id))
        return roi


_api=API()
__version__=api_version() #FIXME: this is probably a bad idea.  The c api version is distinct from the version of this python api

def open(path,stream=None):
    return Reader(path,stream)